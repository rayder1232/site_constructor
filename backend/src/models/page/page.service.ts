import { Injectable, Inject } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Page } from './page.entity';

@Injectable()
export class PageService {
  constructor(
    @Inject('PAGE_REPOSITORY')
    private pageRepository: Repository<Page>,
  ) {}

  async create(page): Promise<Page[]> {
    return this.pageRepository.save(page);
  }

  async getAll(): Promise<Page[]> {
    return this.pageRepository.find();
  }

  async getByName(name: string): Promise<Page> {
    return this.pageRepository.findOne({
      where: {
        name
      }
    })
  }
}
