import { Module } from '@nestjs/common';
import { DatabaseModule } from '../../core/database/database.module';
import { PageController } from './page.controller';
import { pageProviders } from './page.provider';
import { PageService } from './page.service';

@Module({
  imports: [DatabaseModule],
  controllers: [PageController],
  providers: [
    ...pageProviders,
    PageService,
  ],
})
export class PageModule {}
