import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { PageService } from './page.service';

@Controller({
  path: "page",
  version: "1"
})
export class PageController {
  constructor(private readonly pageService: PageService) {}

  @Get("all")
  getAll() {
    return this.pageService.getAll();
  }

  @Get(":name")
  getByName(@Param() params) {
    return this.pageService.getByName(params.name);
  }

  @Post('create')
  create(@Body() page): any {      
    return this.pageService.create(page);
  }
}
