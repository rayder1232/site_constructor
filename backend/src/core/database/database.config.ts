require('dotenv').config();

export = {
  type: process.env.DB_TYPE,
  host: process.env.DB_HOST,
  port: 5433,
  username: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_NAME,
  entities: [
      __dirname + '/../../**/*.entity{.ts,.js}',
  ],
  synchronize: true,
};

