import { createConnection } from 'typeorm';
const databaseConfig = require("./database.config");

export const databaseProviders = [
    {
        provide: "DATABASE_CONNECTION",
        useFactory: async () => await createConnection(databaseConfig),
    },
];
