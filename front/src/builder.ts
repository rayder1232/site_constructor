import './style.scss';

const pageData = [
  {
    cols: [
      {
        number: 6,
        attr: {
          class: "testClass",
          id: "testId"
        },
        inner: [
          {
            tag: "button",
            text: "click me",
            attr: {
              class: "test-button",
              css: {
                "--width": "200px"
              }
            }
          }
        ]
      },
      {
        number: 6,
        attr: {
          class: "testClass",
          id: "testId"
        },
        inner: [
          {
            tag: "button",
            text: "click me",
            attr: {
              class: "testButtonClass"
            }
          }
        ]
      }
    ],
    attr: {
      class: "testRowClass"
    }
  }
]

function buildPage() {
  pageData.forEach(rowData => {
    const $row = $("<div class='row'></div>");

    if (rowData.attr) {
      if (rowData.attr.class) {
        rowData.attr.class += " row";
      }
      $row.attr(rowData.attr);
    }

    if (rowData.cols.length) {
      rowData.cols.forEach(colData => {
        const $col = $(`<div class='col-${colData.number}'></div>`);

        if (colData.attr) {
          if (colData.attr.class) {
            colData.attr.class += " col";
          }
          $col.attr(colData.attr);
        }

        if (colData.inner.length) {
          colData.inner.forEach(elData => {
            // console.log("elData.attr.style", JSON.stringify(elData.attr.style))
            const $el = $(`<${elData.tag}></${elData.tag}>`, {
              text: elData.text,
              ...elData.attr,
              // style: JSON.stringify(elData.attr.style)
            })
            $col.append($el);
          })
        }

        $row.append($col);
      })
    }

    $("#json").append($row);
  })
}

buildPage();
