import './style.scss'
import {DraggableComponent, DroppableComponent} from "./core/DragAndDrop";
import axios from 'axios';

const builder = $('#builder')!;
const app = $('#app')!;

const {data: pageData} = await axios.get("http://localhost:4000/v1/page/index");

$("body").click((e: any) => {
  onBodyClick(e.target);
})

if (!pageData) {
  app.append(createRow());
} else {

}


const clickableElements: any = {
  setChangable: (el: HTMLButtonElement) => setChangable(el),
  removeChangable: (el: HTMLButtonElement) => removeChangable(el),
  adjustChangable: (el: HTMLButtonElement) => adjustChangable(el),
};

function onBodyClick(el: HTMLButtonElement): void {
  if (!el.dataset.click) {    
    return;
  }

  clickableElements[el.dataset.click](el);  
}

function removeChangable(el: HTMLElement): void {
  $(el).closest("[data-changable-active='true']").remove();
}

function adjustChangable(el: HTMLElement): void {
  const adjustTypes: any = {
    col: {
      inputs: [
        {
          attr: {
            type: "number"
          },
          label: "column width",
          "data-click": "addCol",
        }
      ]
    }
  }

  const type = el.dataset.adjust!;
  const $adjustContainer = $("<div class='adjust-container'></div>");

  adjustTypes[type].inputs.forEach((input: any) => {
    const $label = $(`<label>${input.label}</label>`);
    const $input = $("<input/>", input.attr);
    $label.append($input);
    $adjustContainer.append($label);
  })
  
  $(el).closest("[data-changable-active='true']").append($adjustContainer);
}

function setElementVisibleInViewPort(el: any) {
  if (typeof jQuery === "function" && el instanceof jQuery) {
    el = el[0];
  }
  const rect = el.getBoundingClientRect();

  if (rect.right > window.innerWidth) {
    $(el).css("left", rect.left - (rect.right - window.innerWidth - 20));
  }
}

function setChangable(el: HTMLButtonElement) {
  const $el = $(el);

  if ($el.data()["changable-active"]) {
    return;
  }

  const dataAdjust = $el.data().changable;
  
  //TODO: use once
  const $wrapper = $(`
    <div class="changable-wrapper">
      <div class="changable-wrapper__button-block">
        <button class="changable-wrapper__adjust-button" data-click="adjustChangable" data-adjust=${dataAdjust}>
          adjust
        </button>
        <button class="changable-wrapper__close-button" data-click="removeChangable">x</button>
      </div>
    </div>
  `);
  $el.append($wrapper);



  $el.attr("data-changable-active", true);
}

function createRow() {
  return $("<div/>", {
    class: "row row-buider",
    "data-changable": "col", 
    "data-click": "setChangable"
  })
}

const htmlElements = [
  {
    type: "button",
  },
  {
    type: "text",
  }
]

htmlElements.forEach(params => {
  const el = new DraggableComponent(
    $(document.createElement("div"))
  ).element;

  el.text(params.type);
  builder.append(el);
})
