export class DraggableComponent {
  _element: HTMLElement;

  constructor(element: any) {
    if (typeof element === "string") {
      element = $(element);
    }
    element.attr("draggable", "true");
    element.on("dragstart", this._dragstart);
    this._element = element;
  }

  get element() {
    return this._element;
  }

  _dragstart(e: Event) {
    e.dataTransfer.setData("text", e.target.id);
  }
}

export class DroppableComponent {
  _element: HTMLElement;

  constructor(element: any) {
    if (typeof element === "string") {
      element = $(element);
    }

    element.on("drop", this._drop);
    element.on("dragover", this._allowDrop);

    this._element = element;
  }

  _allowDrop(e: Event) {
    e.preventDefault();
  }

  _drop(e: Event) {
    e.preventDefault();
    const data = e.dataTransfer.getData("text");
    e.target.appendChild(document.getElementById(data));
  }
}
